# weather-report-api

As a user running the application  

I can view tomorrow’s predicted temperatures for a given zip-code in the United States

So that I know which will be the coolest hour of the day.

The outputs of this exercise should be a service that gets data from a weather service API and a client interface (could be CLI).  You can use any java framework of your choice.

Considering problem statement, I have tried my best to implement Service API by consuming one of the "accuweather " third party app which allows only 50 calls .

Considering very limited time of weekday and having  very basic knowledge of Spring boot and bit bucket ,I  have tried my best to implement given problem statement.

Currently my application is forecasting next 12 hours weather forecast for US and and predicate overall weather status .but i still believe there can be so many improvement and lots of test cases can be written for the same in futureto improve quality of application .

BitBucket URL:  https://bitbucket.org/atmiksoni/weather-report-api.git
Tools: PostMan. Eclipse, BitBucket ,Embeded tomcat
Technologies : Java, Stream, spring boot ,REST service





You can download code from bit bucket 

git clone https://atmiksoni@bitbucket.org/atmiksoni/weather-report-api.git 
----------------------------------------------------------
Please go through below help to use my application

 Step 1 >  Used one of the open API from  "accuweather" one of the popular app by registering it and using api key.
 
 Link for API:   
http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/1263476?apikey=eCBHnuwrAFpGZ0TYn69Qc7BEPvE92gMU


We can change to 72hour also as per convience


Step 2> By using Spring boot application and Feign client consumed rest application.

======================================================================================= 
How to Run Application
This API has 50 calls/day Limit: 1 key/developer so not sure you will able to acess it.

http://localhost:8090/weather/10001 (where New York City, New York, United States)

/{zipcode}?apikey=eCBHnuwrAFpGZ0TYn69Qc7BEPvE92gMU

Step 3> This will give response as below 


[
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T11:00:00-03:00",
        "atmosphere": "Cloudy",
        "precipitationProbability": 49,
        "temerature": "Temperature:76F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T12:00:00-03:00",
        "atmosphere": "Showers",
        "precipitationProbability": 54,
        "temerature": "Temperature:77F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T13:00:00-03:00",
        "atmosphere": "Cloudy",
        "precipitationProbability": 25,
        "temerature": "Temperature:79F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T14:00:00-03:00",
        "atmosphere": "Cloudy",
        "precipitationProbability": 20,
        "temerature": "Temperature:81F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T15:00:00-03:00",
        "atmosphere": "Cloudy",
        "precipitationProbability": 20,
        "temerature": "Temperature:82F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T16:00:00-03:00",
        "atmosphere": "Intermittent clouds",
        "precipitationProbability": 20,
        "temerature": "Temperature:81F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T17:00:00-03:00",
        "atmosphere": "Intermittent clouds",
        "precipitationProbability": 20,
        "temerature": "Temperature:81F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T18:00:00-03:00",
        "atmosphere": "Mostly sunny",
        "precipitationProbability": 14,
        "temerature": "Temperature:80F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T19:00:00-03:00",
        "atmosphere": "Mostly sunny",
        "precipitationProbability": 1,
        "temerature": "Temperature:77F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T20:00:00-03:00",
        "atmosphere": "Mostly clear",
        "precipitationProbability": 1,
        "temerature": "Temperature:75F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T21:00:00-03:00",
        "atmosphere": "Mostly clear",
        "precipitationProbability": 1,
        "temerature": "Temperature:74F"
    },
    {
        "zipcode": "10001",
        "dateTime": "2019-03-13T22:00:00-03:00",
        "atmosphere": "Partly cloudy",
        "precipitationProbability": 1,
        "temerature": "Temperature:73F"
    }
]

=============================================================

Step 4> Added fallback method when actual webservice is down and it will return empty list.

Step 5-> Added config dependency
here due to time limit constrain i have not added config service module

<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
</dependency>	

Added property
weather-forecast-app.url=https://dataservice.accuweather.com/forecasts/v1/hourly/12hour/

WE can also chnage to 72 hours for 
weather-forecast-app.url=https://dataservice.accuweather.com/forecasts/v1/hourly/72hour/

=====================================

Step 6 >  Adding excpetion handing to our Responsestatus  using

Step 7> Writing very simple logic to predict overall 12 hours weather will be by grouping atmosphere information and
 max value from the response
 
 Below is method  have added tp predicate overall weather in next 12 hours by grouping on atmosphere details (i. sunny, cloudy) and 
 finding max count on that atmosphere

 
 After this changes by response will be look like 
 
 
 {
  "overallWeater" : "Overall Weather Predication will be ->Sunny",
  "miniWeatherreports" : [ {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T07:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "68F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T08:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "70F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T09:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "74F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T10:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "77F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T11:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "80F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T12:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "82F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T13:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "84F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T14:00:00-03:00",
    "atmosphere" : "Mostly sunny",
    "precipitationProbability" : 0,
    "temerature" : "85F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T15:00:00-03:00",
    "atmosphere" : "Mostly sunny",
    "precipitationProbability" : 0,
    "temerature" : "86F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T16:00:00-03:00",
    "atmosphere" : "Mostly sunny",
    "precipitationProbability" : 0,
    "temerature" : "85F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T17:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "83F"
  }, {
    "zipcode" : "10001",
    "dateTime" : "2019-03-14T18:00:00-03:00",
    "atmosphere" : "Sunny",
    "precipitationProbability" : 0,
    "temerature" : "81F"
  } ]
}


============================================================================================================

Step 8 > Adding swagger  

 http://localhost:8090/v2/api-docs
 
 http://localhost:8090/swagger-ui.html
 
 http://localhost:8090/swagger-ui.html#/weather-forecast-controller/getWeatherForeCastByZipcodeUsingGET