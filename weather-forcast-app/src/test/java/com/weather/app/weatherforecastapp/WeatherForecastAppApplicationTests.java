package com.weather.app.weatherforecastapp;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.weather.app.weatherforecastapp.bean.MiniWeatherReport;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherForecastAppApplicationTests {
	
	@Autowired 
	WeatherForecastClient weatherForecastClient;

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void givenWeatherForeCastCient_shouldRunSuccessfully() throws Exception {
		String zipcode ="10001";
		
		 List<MiniWeatherReport> miniweatherReportList = weatherForecastClient.weatherForeCastRequest(zipcode).stream().
	     map( weatherResponse -> {
					MiniWeatherReport miniWeatherReport = new MiniWeatherReport();
					miniWeatherReport.setDateTime(weatherResponse .getDateTime());
					miniWeatherReport.setAtmosphere(weatherResponse.getIconPhrase());
					miniWeatherReport.setTemerature(weatherResponse.getTemperature().getValue() + weatherResponse.getTemperature().getUnit());
					miniWeatherReport.setPrecipitationProbability(weatherResponse.getPrecipitationProbability());
					miniWeatherReport.setZipcode(zipcode);
					return miniWeatherReport; 
				}).collect(Collectors.toList());
	 
	   assertThat(miniweatherReportList.size()).isEqualTo(12);
	   
	   assertEquals(miniweatherReportList.get(0).getZipcode(),zipcode);
	   
	}
	
	

}
