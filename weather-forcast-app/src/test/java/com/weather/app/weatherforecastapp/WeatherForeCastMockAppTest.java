package com.weather.app.weatherforecastapp;

import java.io.IOException;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.weather.app.weatherforecastapp.bean.MiniWeatherReport;
import com.weather.app.weatherforecastapp.util.MockData;
import com.weather.app.weatherforecastapp.util.WeatherForecastUtil;

/**
 * Created by soniiatm on 3/14/2019.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherForeCastMockAppTest {

 

  @Test
  public void testWeatherForeCastData_shouldRetrurnCloudy() throws IOException{
       
    List<MiniWeatherReport> miniWeatherReports =MockData.getWeatherData();
    String predication = WeatherForecastUtil.overallWeatherPredication(miniWeatherReports);
    Assert.assertEquals("Cloudy",predication);

  }

}
