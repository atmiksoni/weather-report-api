package com.weather.app.weatherforecastapp.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by soniiatm on 3/14/2019.
 */
public class WeatherForecastUtilTest {

  @Test
  public void testcontainsOnlyNumbers() throws Exception {
    //when zipcode is valid
    String zipcode ="10001";
    Assert.assertTrue(WeatherForecastUtil.validateRequest(zipcode));

    //when zipcode is not valid
    String invalidZipcode ="1000";
    Assert.assertFalse(WeatherForecastUtil.validateRequest(invalidZipcode));
  }

}