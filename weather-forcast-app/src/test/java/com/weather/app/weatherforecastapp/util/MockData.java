package com.weather.app.weatherforecastapp.util;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import com.google.common.io.Resources;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.weather.app.weatherforecastapp.bean.MiniWeatherReport;

/**
 * Created by soniiatm on 3/14/2019.
 */
public class MockData {


  public static List<MiniWeatherReport> getWeatherData() throws IOException {
    InputStream inputStream = Resources.getResource("weather-data.json").openStream();
    String json = IOUtils.toString(inputStream);
    Type listType = new TypeToken<ArrayList<MiniWeatherReport>>() {
    }.getType();
    List<MiniWeatherReport> weatherReports = new Gson().fromJson(json, listType);
    return weatherReports;
  }
}
