package com.weather.app.weatherforecastapp.service;

import com.weather.app.weatherforecastapp.WeatherForecastClient;
import com.weather.app.weatherforecastapp.bean.MiniWeatherReport;
import com.weather.app.weatherforecastapp.bean.OverAllWeatherPredication;
import com.weather.app.weatherforecastapp.client.WeatherResponse;
import com.weather.app.weatherforecastapp.controller.WeatherForecastController;
import com.weather.app.weatherforecastapp.exception.ZipcodeInvalidException;
import com.weather.app.weatherforecastapp.util.WeatherForecastUtil;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Created by soniiatm on 3/15/2019.
 */
@Service
public class WeatherForecastService {
	
private static final Logger LOGGER = LoggerFactory.getLogger(WeatherForecastService.class);

  @Autowired
  WeatherForecastClient weatherForecastClient;


  public OverAllWeatherPredication getWeatherForeCastServiceByZipcode(String zipcode) {
	  
	 

    List<WeatherResponse> response = weatherForecastClient.weatherForeCastRequest(zipcode);
    
    validate(zipcode);

    LOGGER.info("getWeatherForeCastByZipcode: response={}", response);

    List<MiniWeatherReport> miniweatherReportList = WeatherForecastUtil
        .convertwholeReportToMini(response, zipcode);

    String weatherPredication = WeatherForecastUtil
        .overallWeatherPredication(miniweatherReportList);

    LOGGER.info("{} Overall Weather Predication will be ", weatherPredication);

    return WeatherForecastUtil
        .predictAndMap(miniweatherReportList,
            weatherPredication);

  }

  private void validate(@PathVariable("zipcode") String zipcode) {
    if(!WeatherForecastUtil.validateRequest(zipcode)){
      throw new ZipcodeInvalidException("{} requested zip code::"+zipcode+" is not available");
    }
  }
}
