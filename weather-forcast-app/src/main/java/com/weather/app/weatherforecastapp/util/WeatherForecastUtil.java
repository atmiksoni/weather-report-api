package com.weather.app.weatherforecastapp.util;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.weather.app.weatherforecastapp.bean.MiniWeatherReport;
import com.weather.app.weatherforecastapp.bean.OverAllWeatherPredication;
import com.weather.app.weatherforecastapp.client.WeatherResponse;

public class WeatherForecastUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherForecastUtil.class);

	public WeatherForecastUtil() {
	}
	
	public static  List<MiniWeatherReport> convertwholeReportToMini(List<WeatherResponse> response,String zipcode) {
		List<MiniWeatherReport> miniweatherReportList = response.stream()
					.map( weatherResponse -> {
						MiniWeatherReport miniWeatherReport = new MiniWeatherReport();
						miniWeatherReport.setDateTime(weatherResponse .getDateTime());
						miniWeatherReport.setAtmosphere(weatherResponse.getIconPhrase());
						miniWeatherReport.setTemerature(weatherResponse.getTemperature().getValue() + weatherResponse.getTemperature().getUnit());
						miniWeatherReport.setPrecipitationProbability(weatherResponse.getPrecipitationProbability());
						miniWeatherReport.setZipcode(zipcode);
						return miniWeatherReport; 
					}).collect(Collectors.toList());
		return miniweatherReportList;
	}

	public static boolean validateRequest(String zipcode){
		if(containsOnlyNumbers(zipcode)  && Long.parseLong(zipcode) < 10001){
			return false;
		}
			return true;
	}

	public static boolean containsOnlyNumbers(String str) {
		for (int i = 0; i < str.length(); i++) {
			if (!Character.isDigit(str.charAt(i)))
				return false;
		}
		return true;
	}
	
	public static String overallWeatherPredication(List<MiniWeatherReport> miniReportList) {

		Map<String, Long> weatherReportMap = miniReportList.stream()
				.collect(Collectors.groupingBy(MiniWeatherReport::getAtmosphere, Collectors.counting()));

		weatherReportMap
				.forEach((atmospehere, count) -> LOGGER.info("Atmosphere::" + atmospehere + "::count::" + count));

		// Findingout Max value
		return weatherReportMap.entrySet().stream()
				.max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();

	}
	
	public static  OverAllWeatherPredication predictAndMap(List<MiniWeatherReport> miniweatherReportList,
			String weatherPredication) {
		OverAllWeatherPredication overAllWeatherPredication = new OverAllWeatherPredication();
		overAllWeatherPredication.setOverallWeater("Overall Weather Predication will be ->"+weatherPredication);
		overAllWeatherPredication.setMiniWeatherreports(miniweatherReportList);
		return overAllWeatherPredication;
	}

}