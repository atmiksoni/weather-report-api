package com.weather.app.weatherforecastapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class WeatherForecastAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeatherForecastAppApplication.class, args);
	}

}
