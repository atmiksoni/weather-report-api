package com.weather.app.weatherforecastapp.bean;

import org.springframework.stereotype.Component;

@Component
public class MiniWeatherReport {
	
	private String zipcode;

	private String dateTime;

	private String atmosphere;

	private Integer precipitationProbability;

	private String temerature;

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getAtmosphere() {
		return atmosphere;
	}

	public void setAtmosphere(String atmosphere) {
		this.atmosphere = atmosphere;
	}

	public Integer getPrecipitationProbability() {
		return precipitationProbability;
	}

	public void setPrecipitationProbability(Integer precipitationProbability) {
		this.precipitationProbability = precipitationProbability;
	}

	public String getTemerature() {
		return temerature;
	}

	public void setTemerature(String temerature) {
		this.temerature = temerature;
	}
	
	

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	@Override
	public String toString() {
		return "MiniWeatherReport [dateTime=" + dateTime + ", atmosphere=" + atmosphere
				+ ", precipitationProbability=" + precipitationProbability + ", temerature=" + temerature + ", zipcode=\" + zipcode + \"]";
	}

}
