package com.weather.app.weatherforecastapp;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.weather.app.weatherforecastapp.client.WeatherResponse;

@Component
public class WeatherForecastClientImpl implements WeatherForecastClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherForecastClientImpl.class);
	@Override
	public List<WeatherResponse> weatherForeCastRequest(String zipCpde) {
		LOGGER.info("{} fallback method for weatherForeCast called {} ");
		return Collections.EMPTY_LIST;
	}

}
