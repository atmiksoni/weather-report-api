package com.weather.app.weatherforecastapp.bean;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class OverAllWeatherPredication {
	
	private String overallWeater;
	
	private List<MiniWeatherReport> miniWeatherreports = new ArrayList();

	public String getOverallWeater() {
		return overallWeater;
	}

	public void setOverallWeater(String overallWeater) {
		this.overallWeater = overallWeater;
	}

	public List<MiniWeatherReport> getMiniWeatherreports() {
		return miniWeatherreports;
	}

	public void setMiniWeatherreports(List<MiniWeatherReport> miniWeatherreports) {
		this.miniWeatherreports = miniWeatherreports;
	}
	
	

}
