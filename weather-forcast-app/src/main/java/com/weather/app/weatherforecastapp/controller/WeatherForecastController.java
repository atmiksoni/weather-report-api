package com.weather.app.weatherforecastapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.weather.app.weatherforecastapp.bean.OverAllWeatherPredication;
import com.weather.app.weatherforecastapp.service.WeatherForecastService;

@RestController
public class WeatherForecastController {
	
	@Autowired
	WeatherForecastService weatherForecastService;

	@GetMapping("/weather/{zipcode}")
	public ResponseEntity<OverAllWeatherPredication> getWeatherForeCastByZipcode(@PathVariable ("zipcode") String zipcode) {

		OverAllWeatherPredication overAllWeatherPredication = weatherForecastService.getWeatherForeCastServiceByZipcode(zipcode);

		return  ResponseEntity.ok(overAllWeatherPredication);
		
	}

}
