package com.weather.app.weatherforecastapp.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ResponseStatus(value = HttpStatus.NOT_FOUND,reason="Zipcode is not valid for request")
public class ZipcodeInvalidException extends RuntimeException {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8731503843689277174L;
	
	public ZipcodeInvalidException() {
        super();
    }
    public ZipcodeInvalidException(String message, Throwable cause) {
        super(message, cause);
    }
    public ZipcodeInvalidException(String message) {
        super(message);
    }
    public ZipcodeInvalidException(Throwable cause) {
        super(cause);
    }
}
