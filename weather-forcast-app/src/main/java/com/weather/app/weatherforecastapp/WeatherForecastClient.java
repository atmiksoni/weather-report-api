package com.weather.app.weatherforecastapp;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.weather.app.weatherforecastapp.client.WeatherResponse;

@FeignClient(name="dataservice.accuweather.com", url="${weather-forecast-app.url}",
fallback=WeatherForecastClientImpl.class)
public interface WeatherForecastClient {
	
	
	@GetMapping("/{zipcode}?apikey=eCBHnuwrAFpGZ0TYn69Qc7BEPvE92gMU")
	List<WeatherResponse> weatherForeCastRequest(@PathVariable ("zipcode") String zipCpde) ;
		

}
